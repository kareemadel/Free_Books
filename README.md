# Free Books

Introduction
------------
A library application implemented in python3 using the Django framework.

Prerequisites
-------------
* Python>=3.4
* virtualenv

How to Install
----------
```bash
# clone the repo
$ git clone https://gitlab.com/kareemadel/Free_Books
$ cd Free_Books
# create the virtual environemnt
$ mkdir virtualenvironment
$ virtualenv virtualenvironment
$ source virtualenvironment/bin/activate
# install the dependencies
$ python3 -m pip install -r Library/requirements.txt
```

How to Run the App
-----------------------
```bash
$ python3 Library/manage.py runserver
```
